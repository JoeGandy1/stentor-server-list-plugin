<?php
require 'GameQ/GameQ.php';

if(isset($_POST['action']) && !empty($_POST['action'])) {
    $action = $_POST['action'];
    switch($action) {
        case 'specific' : 
        	specific_request($_POST['ip'],$_POST['port'],$_POST['type'],$_POST['override_names'], $_POST['key']);
        break;
        case 'normal' : 
        	normal_request($_POST['ip'],$_POST['port'], $_POST['key']);
        break;
    }
}

function normal_request($ip, $port, $key){
	$data['status'] = query_normal($ip, $port);
	$data['max_players']="-";
	$data['players']="-";
	if($key)
		$data['key']=$key;
	else
		$data['key']=0;

	echo json_encode($data);
}

function specific_request($ip, $port, $type, $override_names, $key){
	$server_data = query_gameQ($ip, $port, $type);
	$data['status']=$server_data['status'];
	$data['max_players']=$server_data['playersmax'];
	$data['players']=$server_data['players'];
	if(!$override_names){
		$data['name']=preg_replace('/\\\\U0*([0-9a-fA-F]{1,5})/', '&#x\1;', $server_data['name']);
	}
	if($key)
		$data['key']=$key;
	else
		$data['key']=0;

	echo json_encode($data);
}

function query_normal($ip, $port)
	{
			if (! $sock = @fsockopen($ip, $port, $num, $error, 5)){
			    return "Offline";
			}
			else{ 
				return "Online";
				fclose($sock); 
			} 
	}

function query_gameQ($ip, $port, $type){

	$servers = array(
		array(
			'type' => $type,
			'host' => $ip.":".$port,
			),
		);

	$gq = new GameQ;

	$gq->setFilter('normalise');
	$gq->setOption('debug', TRUE);
	
	$gq->addServers($servers);

	try {
	    $data = $gq->requestData();
	    
	    $test = var_export($data, true);
	    //mail ( "joe@gandy.ws" , "Stentor-Server-List-Debug" , $test);

	    if($data[$ip.":".$port]['gq_online'] == 1)
	    	$output['status']="Online";
	    else{
	    	$output['status']="Offline";
	    }
	    $output['playersmax']=$data[$ip.":".$port]['gq_maxplayers'];
	    $output['players']=$data[$ip.":".$port]['gq_numplayers'];

		$strRegex = '/(
            (?:   [\x00-\x7F]                 # single-byte sequences   0xxxxxxx
                |   [\xC0-\xDF][\x80-\xBF]      # double-byte sequences   110xxxxx 10xxxxxx
                |   [\xE0-\xEF][\x80-\xBF]{2}   # triple-byte sequences   1110xxxx 10xxxxxx * 2
                |   [\xF0-\xF7][\x80-\xBF]{3}   # quadruple-byte sequence 11110xxx 10xxxxxx * 3 
            	){1,100}                          # ...one or more times
            )
                | .[0-9|a-f|cX|nY|nX|cY|k-o|r]      # anything else
            /x';

	    $output['name']=$string = preg_replace($strRegex, '$1', $data[$ip.":".$port]['gq_hostname']);
	    return $output;
	}

	// Catch any errors that might have occurred
	catch (GameQ_Exception $e) {
	    echo 'An error occurred.';
	}

}