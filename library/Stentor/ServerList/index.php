<?php
//session_start();
require 'GameQ/GameQ.php';

class Stentor_ServerList_Index
{
	public static function ServerList($hookName, &$contents, array $hookParams, XenForo_Template_Abstract $template)
	{

		$options = XenForo_Application::get('options');

		$end_str = '/library/Stentor/ServerList/request.php';
		
		$url = "";
		
		$url = dirname($_SERVER['HTTP_HOST']);

		if(!$url){
			$url = dirname($_SERVER['PHP_SELF']);
		}
		if(!$url){
			$url = dirname($_SERVER["REQUEST_URI"]);
		}

		$url = $url.$end_str;
		
		switch($hookName){
			case 'ad_sidebar_top':
				$image=array();

				if($url == $end_str){
					$url = $options->stentor_server_list_fallback_url;
				}

				$icons=$options->stentor_server_list_enable_icons;
				$hover=$options->stentor_server_list_enable_hover;
				$override_names=$options->stentor_server_list_override_names;
				$enable_join_urls=$options->stentor_server_list_enable_join_urls;
				$title=$options->stentor_server_list_title;

				$name = explode("\n", $options->stentor_server_names);

				$join_urls = explode("\n", $options->stentor_server_join_urls);

				$ip = explode("\n", $options->stentor_server_ips);

				$port = explode("\n", $options->stentor_server_ports);

				$type = explode("\n", $options->stentor_server_types);
				/*
				$cache_length = 5;
				$date = new DateTime();

				$_SESSION['lastCache'] = isset($_SESSION['lastCache']) ? $_SESSION['lastCache'] : 0 ;

				if($date->getTimestamp() > $_SESSION['lastCache'] + $cache_length){
					$_SESSION['lastCache'] = $date->getTimestamp();
				}
				*/
				if($icons){
					$images = $options->stentor_server_list_icons;
					$image = explode("\n", $images);
				}
				
				foreach ($ip as $key => $value) {
					if($type[$key]=="other" || $type[$key]=="Other"){
						echo
							"<script>".
							"$('#1-$key').text('$name[$key]');".
							"$(this).load(function (){".
							"$.ajax({".
								    "url: '$url',".
									"data: {action: 'normal', ip: '$ip[$key]', port: '$port[$key]'},".
									"type: 'post',".
									"success: function(output){".
										"var arr = $.parseJSON(output);".
										"$('#3-'+$key).text(arr.status);".
										"$('#3-'+$key).addClass(arr.status);".
										"$('#4-'+$key).text(arr.players+'/'+arr.max_players);".
									"}".
								"});".
							"});</script>";
					}
					elseif($type[$key] != ''){
						echo
							"<script>".
							"$('#1-$key').text('$name[$key]');".
							"$(this).load(function (){".
							"$.ajax({".
								    "url: '$url',".
									"data: {action: 'specific', ip: '$ip[$key]', port: '$port[$key]', type: '$type[$key]', override_names: '$override_names'},".
									"type: 'post',".
									"success: function(output){".
										"var arr = $.parseJSON(output);".
										"if(arr.name){".
											"$('#1-'+$key).text(arr.name);".
										"}".
										"$('#3-'+$key).text(arr.status);".
										"$('#3-'+$key).addClass(arr.status);".
										"$('#4-'+$key).text(arr.players+'/'+arr.max_players);".
									"}".
								"});".
							"});</script>";
					}
				
				}

				$contents .= $template->create('stentor_server_list_maintemplate', array(
					'name' => $name,
					'ip'   => $ip,
					'port' => $port,
					'status' => $status,
					'join_urls' => $join_urls,
					'enable_join_urls' => $enable_join_urls,
					'type' => $type,
					'max_players' => $max_players,
					'players' => $players,
					'image' => $image,
					'icons_enable' => $icons,
					'hover' => $hover,
					'title' => $title
					));
			break;
		}
	}

}